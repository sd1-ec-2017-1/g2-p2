/*
  Adiciona <mensagem> no <elemento_id>.
*/
function adiciona_mensagem(mensagem,elemento_id,timestamp) {
	var novo_elemento = document.createElement('div');
	novo_elemento.id = "mensagem"+timestamp;
	document.getElementById(elemento_id).appendChild(novo_elemento);
	document.getElementById(novo_elemento.id).innerHTML=mensagem;
}

/*
  Transforma timestamp em formato HH:MM:SS
*/
function timestamp_to_date( timestamp ) {
	var date = new Date( timestamp );
	var hours = date.getHours();
	var s_hours = hours < 10 ? "0"+hours : ""+hours;
	var minutes = date.getMinutes();
	var s_minutes = minutes < 10 ? "0"+minutes : ""+minutes;
	var seconds = date.getSeconds();
	var s_seconds = seconds < 10 ? "0"+seconds : ""+seconds;
	return s_hours + ":" + s_minutes + ":" + s_seconds;
}
//------------------------------------------------------------------------------------------------
function iniciar(elemento_id,alteraNick) {
	$("#status").text("Conectado - irc://"+
			Cookies.get("nick")+"@"+
			Cookies.get("servidor")+"/"+
			Cookies.get("canal"));
	carrega_mensagens(elemento_id,0);
	carrega_usuario();
}

/*
  Carrega as mensagens ocorridas após o <timestamp>,
  acrescentando-as no <elemento_id>
*/
var novo_timestamp="0";
function carrega_mensagens(elemento_id, timestamp) {
	var mensagem = "";
	var horario = "";
	$.get("obter_mensagem/"+timestamp, function(data,status) {
		if ( status == "success" ) {
		    var linhas = data;
		    for ( var i = linhas.length-1; i >= 0; i-- ) {
		    	horario = timestamp_to_date(linhas[i].timestamp);
			mensagem =
				"["+horario+" - "+
				linhas[i].nick+"]: "+
		                linhas[i].msg;
			novo_timestamp = linhas[i].timestamp;
		    	adiciona_mensagem(mensagem,elemento_id,novo_timestamp);
			}
		}
		else {
		    alert("erro: "+status);
		}
		}
	);
	t = setTimeout(
		function() {
			carrega_mensagens(elemento_id,novo_timestamp)
		},
		1000);
}

/*
   Submete a mensagem dos valores contidos s elementos identificados
   como <elem_id_nome> e <elem_id_mensagem>
*/
function submete_mensagem(elem_id_mensagem) {
	var mensagem= document.getElementById(elem_id_mensagem).value;
  if (mensagem[0]!='/'){
	var msg = '{"timestamp":'+Date.now()+','+
	'"nick":"'+Cookies.get("nick")+'",'+
	'"msg":"'+mensagem+'"}';
	$.ajax({
		type: "post",
		url: "/gravar_mensagem",
		data: msg,
		success:
		function(data,status) {
			if (status == "success") {
				// nada por enquanto
			}
			else {
				alert("erro:"+status);
			}
		},
		contentType: "application/json",
		dataType: "json"
	});
}else {
	comandos(mensagem,elem_id_mensagem);
}
	document.getElementById(elem_id_mensagem).value = "";
}

/*
função que vai lidar com os comandos
recebe a mensagem digitada pelo cliente
*/
function comandos(mensagem){
		var comando = mensagem.split(" ");
		switch (comando[0]) {
			case "/list":
				$.get("comando/lista_canais",function(data,status){
					var terminou = data;
						if(!terminou)
							carrega_lista();
				});
				break;
			case "/nick":
				var arg = comando[1];
				$.get("comando/mudar_nick/"+arg+"",function(data,status){
					Cookies.set('nick', data);
					iniciar("mensagem");
			});
			break;
			case "/join":
				var arg = comando[1];
				$.get("comando/entrar_canal/"+arg+"",function(data,status){
					Cookies.set('canal', data);
					iniciar("mensagem");
			});
			break;
			default:
		}
}
/*
*Função responsavel por carregar a lista quando estiver pronta
*ela chama ela mesma até a lista estiver pronta
*/
function carrega_lista(){
		$.get("obter_lista",function(data,status){
			if(!data){
				carrega_lista();
			}else {
				var lista = data;
				console.log(lista[1].name);
				for (var i=0; i<lista.length;i++){
					var node = document.createElement("LI");
					var textnode = document.createTextNode(JSON.stringify(lista[i]));
					node.appendChild(textnode);
					document.getElementById("lista1").appendChild(node);
				}
			}

		});
}
/*
* Função responsavel pela lista de usuarios ativos
*/
function carrega_usuario(){
		$.get("obter_list_usuario", function(data,status){
				var lista;
				if (data != "undefined"){
					document.getElementById("estado").innerHTML = "Usuarios conectados";
					for(var i =0; i<data.length; i++){
					//	lista += data[i]+"";
					var node = document.createElement("LI");
					var textnode = document.createTextNode(data[i]);
					node.appendChild(textnode);
					document.getElementById("lista").appendChild(node);
				}

			}else{
						document.getElementById("estado").innerHTML = "Carregando lista de usuarios";
				}
			});
				$('ul').empty();
		 t = setTimeout(function(){
		 carrega_usuario();
	 },1000);
}

function initChannels()
{
	var channels = ['foo', 'bar', 'foobar', 'foo', 'bar', 'foobar', 'foo', 'bar', 'foobar'];

	var nav = $('#nav-container');

	for(var i = 0; i < channels.length; i++)
	{
		nav.append('<div class="btn-channel"><button class="" onclick="toggleChannel(this)" id="' + channels[i] + '">#' + channels[i] + '</button></div>');
	}
}

function toggleChannel(el)
{
	var id = el.innerHTML;

	$('#nav-container').children().removeClass("active");
	$(id).addClass("active")

	$('#chnl-board').children().css('display', 'none');
  $(id).css('display', 'inherit');
}
